import 'package:note_pad/database/DbHelper.dart';
import 'package:note_pad/database/Util.dart';
import 'package:note_pad/model/note_model.dart';

class NoteRepository {
  DbHelper dbHelper = DbHelper();

  Future<List<NoteModel>> getNotes() async {
    var dbClient = await dbHelper.db;
    List<Map> maps = await dbClient.query(tableName, columns: ['id', 'note']);
    List<NoteModel> noteList = [];
    for (int i = 0; i < maps.length; i++) {
      noteList.add(NoteModel.fromMap(maps[i]));
    }
    return noteList;
  }

  Future<List<NoteModel>> getCompletes() async {
    var dbClient = await dbHelper.db;
    List<Map> maps2 = await dbClient.query(tableName2, columns: ['id', 'note']);
    List<NoteModel> completedList = [];
    for (int i = 0; i < maps2.length; i++) {
      completedList.add(NoteModel.fromMap(maps2[i]));
    }
    return completedList;
  }

  Future<int> add(NoteModel noteModel) async {
    var dbClient = await dbHelper.db;
    return await dbClient.insert(tableName, noteModel.toMap());
  }

  Future<int> addCompletes(NoteModel noteModel) async {
    var dbClient = await dbHelper.db;
    return await dbClient.insert(tableName2, noteModel.toMap());
  }

  Future<int> update(NoteModel noteModel) async {
    var dbClient = await dbHelper.db;
    return await dbClient.update(tableName, noteModel.toMap(),
        where: 'id = ?', whereArgs: [noteModel.id]);
  }

  Future<int> delete(int id) async {
    var dbClient = await dbHelper.db;
    return await dbClient.delete(tableName, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> completeDelete(int id) async {
    var dbClient = await dbHelper.db;
    return await dbClient.delete(tableName2, where: 'id = ?', whereArgs: [id]);
  }
}
