import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;

class DbHelper {
  static Database? _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }
    _db = await initDatabase();
    return _db!;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    print(documentDirectory.path);
    String path = join(documentDirectory.path, 'note.db');
    var db = openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) {
    db.execute("CREATE TABLE mynote(id INTEGER PRIMARY KEY, note TEXT)");
    db.execute("CREATE TABLE completednote(id INTEGER PRIMARY KEY, note TEXT)");
  }
}
