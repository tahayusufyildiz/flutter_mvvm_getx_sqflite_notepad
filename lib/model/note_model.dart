class NoteModel {
  int? id;
  String? note;

  NoteModel({this.id, this.note});

  factory NoteModel.fromMap(Map<dynamic, dynamic> json) {
    return NoteModel(
      id: json['id'],
      note: json['note'],
    );
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'note': note,
    };
  }
}
