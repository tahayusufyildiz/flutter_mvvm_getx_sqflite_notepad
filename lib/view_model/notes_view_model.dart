import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';
import 'package:note_pad/model/note_model.dart';
import 'package:note_pad/repositories/note_repositories.dart';

class NotePageViewModel extends GetxController {
  var allNotes = <NoteModel>[].obs;
  var allCompletes = <NoteModel>[].obs;
  NoteRepository noteRepository = NoteRepository();
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchAllNote();
  }

  fetchAllNote() async {
    var note = await noteRepository.getNotes();
    allNotes.value = note;
  }

  addNote(NoteModel noteModel) {
    noteRepository.add(noteModel);
    fetchAllNote();
  }

  updateNote(NoteModel noteModel) {
    noteRepository.update(noteModel);
    fetchAllNote();
  }

  deleteNote(int id) {
    noteRepository.delete(id);
    fetchAllNote();
  }
}
