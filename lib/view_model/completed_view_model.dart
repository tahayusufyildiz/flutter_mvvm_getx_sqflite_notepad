import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';
import 'package:note_pad/model/note_model.dart';
import 'package:note_pad/repositories/note_repositories.dart';

class CompletedPageViewModel extends GetxController {
  var allCompletes = <NoteModel>[].obs;
  var allNotes = <NoteModel>[].obs;
  NoteRepository noteRepository = NoteRepository();
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    fetchAllCompleted();
  }

  fetchAllCompleted() async {
    var note = await noteRepository.getCompletes();
    allCompletes.value = note;
  }

  addtoComleted(NoteModel noteModel) {
    noteRepository.addCompletes(noteModel);
    fetchAllCompleted();
  }

  deleteCompletedNote(int id) {
    noteRepository.completeDelete(id);
    fetchAllCompleted();
  }
}
