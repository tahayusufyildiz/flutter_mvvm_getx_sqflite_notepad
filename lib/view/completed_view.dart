import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:note_pad/model/note_model.dart';
import 'package:note_pad/view_model/completed_view_model.dart';
import 'package:note_pad/view_model/notes_view_model.dart';

class CompletedPage extends StatefulWidget {
  const CompletedPage({Key? key}) : super(key: key);

  @override
  State<CompletedPage> createState() => _CompletedPageState();
}

class _CompletedPageState extends State<CompletedPage> {
  final completedPageVievModel = Get.put(CompletedPageViewModel());
  final notePageViewModel = Get.put(NotePageViewModel());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow.shade100,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.red),
        centerTitle: true,
        backgroundColor: Colors.yellow.shade200,
        title: Text(
          "CompletedTasks",
          style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        ),
        actions: [
          TextButton(
            onPressed: () {
              print(completedPageVievModel.allCompletes.length.toString());
              for (int i = 0;
                  i < completedPageVievModel.allCompletes.length;
                  i++) {
                completedPageVievModel.deleteCompletedNote(
                    completedPageVievModel.allCompletes[i].id!);
              }
            },
            child: Text(
              "DeleteAll",
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
      body: Obx(
        () => Container(
            child: completedPageVievModel.allCompletes.length != 0
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            completedPageVievModel.allCompletes.length
                                    .toString() +
                                " completed task ",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: completedPageVievModel.allCompletes.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 100,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(22),
                                    color: Colors.lightGreen.shade400,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 5,
                                        offset: Offset(0, 3),
                                      ),
                                    ]),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        completedPageVievModel
                                            .allCompletes[index].note!,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 22),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        IconButton(
                                            onPressed: () {
                                              notePageViewModel.addNote(
                                                  NoteModel(
                                                      id: null,
                                                      note:
                                                          completedPageVievModel
                                                              .allCompletes[
                                                                  index]
                                                              .note!));
                                              completedPageVievModel
                                                  .deleteCompletedNote(
                                                      completedPageVievModel
                                                          .allCompletes[index]
                                                          .id!);
                                              notePageViewModel.fetchAllNote();
                                              Get.snackbar("Succeed",
                                                  "The task has been re-added.");
                                            },
                                            icon: Icon(
                                              Icons.refresh,
                                              color: Colors.red,
                                              size: 32,
                                            ))
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  )
                : Center(child: Lottie.asset('assets/16656-empty-state.json'))),
      ),
    );
  }
}
