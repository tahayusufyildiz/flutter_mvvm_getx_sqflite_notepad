import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:note_pad/model/note_model.dart';
import 'package:note_pad/view_model/completed_view_model.dart';
import 'package:note_pad/view_model/notes_view_model.dart';

import 'completed_view.dart';

class NotePage extends StatefulWidget {
  const NotePage({Key? key}) : super(key: key);

  @override
  State<NotePage> createState() => _NotePageState();
}

class _NotePageState extends State<NotePage> {
  final notePageViewModel = Get.put(NotePageViewModel());
  final completedPageViewModel = Get.put(CompletedPageViewModel());
  final _noteController = TextEditingController();
  int? noteId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.yellow.shade100,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.yellow.shade200,
          title: Text(
            "ShoppingList",
            style: TextStyle(color: Colors.red),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(CompletedPage());
                },
                icon: Icon(
                  Icons.delete,
                  color: Colors.red,
                ))
          ],
        ),
        body: Obx(
          () => SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: _noteController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "NOTE",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        ),
                        suffixIcon: Icon(Icons.note_add),
                        suffixIconColor: Colors.red,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_noteController.text != "") {
                              notePageViewModel.addNote(NoteModel(
                                  id: null, note: _noteController.text));
                              _noteController.text = "";
                            } else {
                              Get.snackbar("Error", "Empty note",
                                  colorText: Colors.red);
                            }
                          },
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.yellow.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_noteController.text != "") {
                              notePageViewModel.updateNote(NoteModel(
                                  id: noteId, note: _noteController.text));
                              _noteController.text = "";
                            } else {
                              Get.snackbar("Error", "Empty note",
                                  colorText: Colors.red);
                            }
                          },
                          child: Text(
                            "UPDATE",
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.yellow.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  notePageViewModel.allNotes.length != 0
                      ? Expanded(
                          child: ListView.builder(
                            itemCount: notePageViewModel.allNotes.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                  onTap: () {
                                    noteId =
                                        notePageViewModel.allNotes[index].id!;
                                    _noteController.text =
                                        notePageViewModel.allNotes[index].note!;
                                  },
                                  child: Container(
                                    height: 100,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.yellow.shade200,
                                      borderRadius: BorderRadius.circular(22),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 5,
                                          offset: Offset(0, 3),
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            notePageViewModel
                                                .allNotes[index].note!,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 22),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: IconButton(
                                              onPressed: () {
                                                completedPageViewModel
                                                    .addtoComleted(NoteModel(
                                                        id: null,
                                                        note: notePageViewModel
                                                            .allNotes[index]
                                                            .note!));

                                                notePageViewModel.deleteNote(
                                                    notePageViewModel
                                                        .allNotes[index].id!);
                                              },
                                              icon: Icon(
                                                Icons.delete,
                                                color: Colors.red,
                                                size: 32,
                                              )),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      : Lottie.asset("assets/16656-empty-state.json"),
                ],
              ),
            ),
          ),
        ));
  }
}
